    <?php 

require '../vendor/autoload.php';

use phpunit\framework\TestCase;
use Decoupled\Core\Extension\Bundle\BundleExtension;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Extension\Output\OutputExtension;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\DependencyInjection\DependencyInjectionExtension;
use Decoupled\Core\Bundle\Process\ViewProcess;
use Decoupled\Core\Bundle\Process\Test\AppBundle;

class ProcessTest extends TestCase{

    public function testCanUseProcess()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new BundleExtension() );

        $app->uses( new OutputExtension() );

        $app->uses( new DependencyInjectionExtension() );

        $app->bundle()->add( new AppBundle() );

        $app->uses(function( $bundleInitializer ){

            $bundleInitializer->uses( new ViewProcess($this) );
        });

        $app->process()->init( $app->bundle() );

        $output = (string) $app->output( '@app.bundle/view.html.twig' )->with('value', 'foo');

        $this->assertContains( 'foo', $output );

        $output2 = (string) $app->output( '@app.bundle/view.php' )->with('value', 'foo');

        $this->assertEquals( $output, $output2 ); 
    }

}