<?php namespace Decoupled\Core\Bundle\Process;

use Decoupled\Core\Bundle\BundleProcess;
use Decoupled\Core\Bundle\BundleInterface;
use Decoupled\Core\Application\ApplicationContainer;

class ViewProcess extends BundleProcess{

    public function __construct( ApplicationContainer $app )
    {
        $this->setApp( $app );
    }

    public function getName()
    {
        return 'view.process';
    }

    public function process( BundleInterface $bundle )
    {
        $dir = $bundle->getDir().DIRECTORY_SEPARATOR.'views';

        if( !file_exists($dir) ) return;

        $app = $this->getApp();

        $inflect = $app['$inflector'];

        $loader = $app['$template.loader'];

        $bundleKey = $inflect( $bundle->getName() )->asKey();        

        $loader->addPath( $dir, $bundleKey );
    }
}